<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Exception\RequestException;
use App\Exception\ApiException;

class ApiService
{
    
    private $client;
    
    public function __construct(ContainerInterface $container)
    {
        $this->client = $container->get('eight_points_guzzle.client.products');
    }
    
    public function getAllProducts($filter)
    {
        if ($filter == 1) {
            $amountFilter = 'gt:0';
        } elseif($filter == 2) {
            $amountFilter = 'e:0';
        } elseif($filter == 3) {
            $amountFilter = 'gt:5';
        } else {
            $amountFilter = '';
        }
        
        try {
            $response = $this->client->get('/products', ['query' => ['amount' => $amountFilter]]);
            
            $code = $response->getStatusCode();
            $rawBody = $response->getBody();
            $body = json_decode($rawBody, true);
            
            return $body['items'] ?? [];
        } catch(RequestException $e) {
            if ($e->hasResponse()) {
                $rawBody = $e->getResponse()->getBody();
                $body = json_decode($rawBody, true);
                $error = $body['error'] ?? 'undefined error';
            }
            $error = $body['error'] ?? 'undefined error';
            throw new ApiException($error);
        }
    }
    
    public function getProduct($id) 
    {
        try {
            $response = $this->client->get('/products/' . $id);
            
            $rawBody = $response->getBody();
            $item = json_decode($rawBody, true);
            
            return $item;
        } catch(RequestException $e) {
            if ($e->hasResponse()) {
                $rawBody = $e->getResponse()->getBody();
                $body = json_decode($rawBody, true);
                $error = $body['error'] ?? 'undefined error';
            }
            $error = $body['error'] ?? 'undefined error';
            throw new ApiException($error);
        }
    }
    
    public function createProduct($data) 
    {
        try {
            $response = $this->client->post('/products', [
                'form_params' => [
                    'name' => $data['name'],
                    'amount' => $data['amount'],
                ]
            ]);
            
            return true;
        } catch(RequestException $e) {
            if ($e->hasResponse()) {
                $rawBody = $e->getResponse()->getBody();
                $body = json_decode($rawBody, true);
                $error = $body['error'] ?? 'undefined error';
            }
            $error = $body['error'] ?? 'undefined error';
            throw new ApiException($error);
        }
        
        return false;
    }
    
    public function updateProduct($id, $data) 
    {
        try {
            $response = $this->client->put('/products/' . $id, [
                'form_params' => [
                    'name' => $data['name'],
                    'amount' => $data['amount'],
                ]
            ]);
            
            return true;
        } catch(RequestException $e) {
            if ($e->hasResponse()) {
                $rawBody = $e->getResponse()->getBody();
                $body = json_decode($rawBody, true);
                $error = $body['error'] ?? 'undefined error';
            }
            $error = $body['error'] ?? 'undefined error';
            throw new ApiException($error);
        }
        
        return false;
    }
    
    public function deleteProduct($id) 
    {
        try {
            $response = $this->client->delete('/products/' . $id);
        } catch(RequestException $e) {
            if ($e->hasResponse()) {
                $rawBody = $e->getResponse()->getBody();
                $body = json_decode($rawBody, true);
                $error = $body['error'] ?? 'undefined error';
            }
            $error = $body['error'] ?? 'undefined error';
            throw new ApiException($error);
        }
    }
    
}