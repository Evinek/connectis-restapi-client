<?php
namespace App\Exception;

use Exception;

class ApiException extends Exception
{

    public function __construct($message = null)
    {
        parent::__construct($message);
    }
}