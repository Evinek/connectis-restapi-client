<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use App\Form\ItemForm;
use App\Service\ApiService;
use App\Exception\ApiException;

class ProductController extends Controller
{
    /**
     * @Route("/", name="products")
     */
    public function index(Request $request, ApiService $apiService)
    {
        $filter = $request->get('filter', 0);
        
        $products = $apiService->getAllProducts($filter);
        
        return $this->render('index.html.twig', ['products' => $products]);
    }
    
    /**
     * @Route("/create", name="products_create")
     */
    public function create(Request $request, ApiService $apiService)
    {
        $error = null;
        $form = $this->createForm(ItemForm::class, ['amount' => 1]);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            
            try {
                $apiService->createProduct($data);
                
                return $this->redirectToRoute('products');
            } catch(ApiException $e) {
                $error = $e->getMessage();
            }
        }
        
        return $this->render('product_edit.html.twig', array(
            'form' => $form->createView(),
            'error' => $error,
            'type' => 'add',
        ));
    }
    
    /**
     * @Route("/edit/{id}", name="products_edit", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $id, ApiService $apiService)
    {
        try {
            $item = $apiService->getProduct($id);
        } catch(ApiException $e) {
            throw $this->createNotFoundException($e->getMessage());
        }
        
        $error = null;
        $form = $this->createForm(ItemForm::class, ['name' => $item['name'], 'amount' => $item['amount']]);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            
            try {
                $apiService->updateProduct($id, $data);
                
                return $this->redirectToRoute('products');
            } catch(ApiException $e) {
                $error = $e->getMessage();
            }
        }
        
        return $this->render('product_edit.html.twig', array(
            'form' => $form->createView(),
            'error' => $error,
            'type' => 'edit',
        ));
    }
    
    /**
     * @Route("/delete/{id}", name="products_delete", requirements={"id"="\d+"})
     */
    public function delete(Request $request, $id, ApiService $apiService)
    {
        if (!$this->isCsrfTokenValid('delete-item', $request->get('token'))) {
            return $this->redirectToRoute('products', ['error' => 'invalid token']);
        }
        
        try {
            $apiService->deleteProduct($id);
        } catch(ApiException $e) {
            $error = $e->getMessage();
            return new Response($error);
        }

        return $this->redirectToRoute('products');
    }
    
}
